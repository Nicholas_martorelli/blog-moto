<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\MagazineController;
use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'home'])->name('home');


Route::post('/submit', [PublicController::class, 'submit'])->name('submit');

Route::get('/magazine/create', [MagazineController::class, 'create'])->name('magazine.create');

Route::post('/magazine/store', [MagazineController::class, 'store'])->name('magazine.store');

Route::get('/magazine/index', [PublicController::class, 'index'])->name('magazine.index');

Route::get('/magazine/show/{magazine}', [PublicController::class, 'show'])->name('magazine.show');

Route::get('/article/create',[ArticleController::class, 'create'])->name('article.create');

Route::post('/article/store',[ArticleController::class, 'store'])->name('article.store');

Route::get('/article/index',[PublicController::class, 'indexArticle'])->name('article.index');

Route::get('/article/show/{article}',[PublicController::class, 'showArticle'])->name('article.show');

Route::get('article/edit/{article}',[ArticleController::class, 'edit'])->name('article.edit');

Route::put('/article/update/{article}',[ArticleController::class, 'update'])->name('article.update');

Route::delete('/article/destroy/{article}', [ArticleController::class, 'destroy'])->name('article.destroy');

Route::get('article/auth/{auth}',[ArticleController::class, 'auth'])->name('article.auth');

