<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Magazine;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function home() {
        return view('welcome');
    }

    public function submit(Request $req){
        $user = $req->input('user');
        $message = $req->input('message');

        $email = $req->input('email');

        $contact = compact('user' , 'message');
        
        
        Mail::to($email)->send(new ContactMail($contact));
        return redirect(route('home'))->with('message','la tua richiesta è stata inoltrata');
    }
    public function index()
    {
        $magazines = Magazine::all();
        return view('magazine.index', compact('magazines'));
    }

    public function show(Magazine $magazine)
    {
        return view('magazine.show', compact('magazine'));
    }


    public function showArticle(Article $article)
    {
        return view('article.show',compact('article'));
    }

    public function indexArticle()
    {
        $articles = Article::all();
        return view('article.index', compact('articles'));
    }
}
