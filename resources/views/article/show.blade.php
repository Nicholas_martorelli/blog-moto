<x-layout>


    
    
    <div class="container m-top-100">
        <div class="row">
            <div class="col-12">
                <h1 class="main-color text-center"> Vendo {{$article->title}}</h1>
            </div>
        </div>
        <div class="col-md-6 offset-md-3 my-5 img-fluid">
            @if ($article->img)
                
            <img src="{{Storage::url($article->img)}}" class="card-img-top" alt="...">
            @else
    
                
            <img src="/img/moto-icona.png" alt="">
    
            @endif
        </div>
        <div class="col-12 mt-5">
            <h5 class="black-color shadow card px-4 py-4 fw-light">{{$article->description}}</h5>
        </div>
        <div class="col-12 mt-5">
            <h5 class="black-color">Per info scrivetemi: {{$article->email}} <i class="fas fa-envelope main-color"></i></h5>
        </div>
        <div class="text-center my-5">
            <a href="{{route('article.index',compact('article'))}}" class="btn button me-5 rounded-pill">Torna agli articoli!</a>
            @if ($article->user->id==Auth::user()->id)
            <a href="{{route('article.edit',compact('article'))}}" class="btn button rounded-pill">Modifica</a>
            <form method="POST" action="{{route('article.destroy', compact('article'))}}">
            @csrf
            @method('delete')
            <button class="btn btn-danger rounded-pill mt-3" type="submit"> cancella</button>
            </form>
            @endif
        </div>
    </div>











</x-layout>