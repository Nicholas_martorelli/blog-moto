<x-layout>
  
    
    <div class="container  m-top-100 m-bot-100">
        <div class="card border-0 shadow my-5">
          <div class="card-body p-5">
            <h3 class="text-center fw-bold main-color mb-3">
                Modifica il tuo Annuncio!
            </h3>
            <div >
              @if ($errors->any())
            <div class="alert alert-danger">
               <ul>
                   @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                   @endforeach
               </ul>
             </div>
              @endif
                <form method="POST" action="{{route('article.update', compact('article'))}}" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label main-color">Titolo </label>
                      <input name="title"type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$article->title}}">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail2" class="form-label main-color">Email </label>
                        <input name="email"type="text" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" value="{{$article->email}}">
                      </div>
                    <div class="mb-3">
                        <img src="{{Storage::url($article->img)}}" class="img-fluid" alt="">
                        <label for="image" class="form-label main-color">Immagine</label>
                        <input name="img"type="file" class="form-control" id="image" aria-describedby="emailHelp">
                      </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label main-color">Descrizione</label>
                      <textarea name="description" class="form-control" id="exampleInputPassword1" cols="30" rows="7">{{$article->description}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-success">Modifica</button>
                </form>
            </div>
          </div>
        </div>
    </div>
















</x-layout>