<x-layout>


    <div class="container text-center m-top-100 m-bot-100">
        <div class="row">
            <div class="col-12">
                <h1 class="main-color">gli annunci di {{$user->name}}!</h1>
                <p class="fw-bold">Cerca quello che fa per te!</p>
            </div>
        </div>
        <div class="row">
            @foreach ($articles as $article)
             
            <div class="col-12 col-md-6 mt-5">
                <div class="card">
                    @if ($article->img)
                
                    <img src="{{Storage::url($article->img)}}" class="card-img-top" alt="...">
                    @else
            
                        
                    <img src="/img/moto-icona.png" alt="">
            
                    @endif
                    <div class="card-body">
                        <h5 class="card-title main-color my-4">{{$article->title}}</h5>
                        <p class="my-1">Aunnuncio di <a href="{{route('article.auth',['auth'=>$article->user->id])}}">{{$article->user->name}}</a> </p>
                        <a href="{{route('article.show',compact('article'))}}" class="btn button mt-1">Dettagli</a>
                    </div>
                </div>
            </div>
            
          @endforeach
        </div>
    </div>
    
    
    
    </x-layout>