<x-layout>


<div class="container text-center m-top-100 m-bot-100">
    <div class="row">
        <div class="col-12">
            <h1 class="main-color">I Nostri Articoli</h1>
        </div>
    </div>
    <div class="row">
        @foreach ($magazines as $magazine)
         
        <div class="col-12 col-md-6 mt-5">
            <div class="card">
                @if ($magazine->img)
            
                <img src="{{Storage::url($magazine->img)}}" class="card-img-top" alt="...">
                @else
        
                    
                <img src="/img/moto-icona.png" alt="">
        
                @endif
                <div class="card-body">
                    <h5 class="card-title main-color my-4">{{$magazine->title}}</h5>
                    <a href="{{route('magazine.show',compact('magazine'))}}" class="btn button">Dettagli</a>
                </div>
            </div>
        </div>
        
      @endforeach
    </div>
</div>



</x-layout>