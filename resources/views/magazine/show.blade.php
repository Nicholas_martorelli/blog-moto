<x-layout>


    
    
    <div class="container m-top-100">
        <div class="row">
            <div class="col-12">
                <h1 class="main-color text-center">{{$magazine->title}}</h1>
            </div>
        </div>
        <div class="col-md-6 offset-md-3 my-5 img-fluid">
            @if ($magazine->img)
                
            <img src="{{Storage::url($magazine->img)}}" class="card-img-top" alt="...">
            @else
    
                
            <img src="/img/moto-icona.png" alt="">
    
            @endif
        </div>
        <div class="col-12 mt-5">
            <h5 class="black-color">{{$magazine->description}}</h5>
        </div>
        <div class="text-center my-5">
            <a href="{{route('magazine.index',compact('magazine'))}}" class="btn button ">Torna agli articoli!</a>
        </div>
    </div>











</x-layout>