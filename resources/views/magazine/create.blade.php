<x-layout>
  
    
    <div class="container  m-top-100 m-bot-100">
        <div class="card border-0 shadow my-5">
          <div class="card-body p-5">
            <h3 class="text-center fw-bold main-color mb-3">
                Crea il tuo Articolo!
            </h3>
            <div >
              @if ($errors->any())
            <div class="alert alert-danger">
               <ul>
                   @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                   @endforeach
               </ul>
             </div>
              @endif
                <form method="POST" action="{{route('magazine.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label main-color">Titolo Articolo</label>
                      <input name="title"type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                        <label for="image" class="form-label main-color">Immagine</label>
                        <input name="img"type="file" class="form-control" id="image" aria-describedby="emailHelp">
                      </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label main-color">Descrizione</label>
                      <textarea name="description" class="form-control" id="exampleInputPassword1" cols="30" rows="7"></textarea>
                    </div>
                    <button type="submit" class="btn btn-success">Crea</button>
                </form>
            </div>
          </div>
        </div>
    </div>






{{-- <div style="height: 150px"></div> --}}









</x-layout>