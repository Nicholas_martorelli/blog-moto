<x-layout>


<div class="continer m-top-100 m-bot-100">
    <h1 class="main-color text-center">Registrati</h1>
</div>



<div class="container m-bot-100 card border-0 shadow px-5 py-5">
    <div class="row">
        <div class="col-12">
            <form method="POST" action="{{route('register')}}">
                @csrf
                <div class="mb-3 mt-5">
                    <label for="name1" class="form-label">Nome utente</label>
                    <input type="text" name="name"class="form-control" id="name1" aria-describedby="emailHelp">
                  </div>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Email </label>
                  <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Password</label>
                  <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                </div>
                <div class="mb-3">
                    <label for="password_confirmation" class="form-label">conferma password</label>
                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
                  </div>
                <button type="submit" class="btn button">Iscriviti</button>
            </form>
        </div>
    </div>
</div>

















</x-layout>