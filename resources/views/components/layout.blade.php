<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--mio style e quello di bootstrap-->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <!--link font awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <!--google font-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">


    <title>Moto.Blog</title>
</head>
<body>
  
    
    <nav class="navbar navbar-expand-lg  px-5 nav">
        <div class="container-fluid">
          <a class="navbar-brand" href="{{route('home')}}">Moto.Blog</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fas fa-motorcycle grey-color"></i></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                @guest
                <li class="nav-item">
                    <a class="nav-link " aria-current="page" href="{{route('home')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('login')}}">Login</a>
                </li> 
                <li class="nav-item">
                    <a class="nav-link" href="{{route('register')}}">Register</a>
                </li>     
                @else
                <li class="nav-item">
                  <a class="nav-link " aria-current="page" href="{{route('home')}}">Home</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Ciao {{Auth::user()->name}}
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><a class="dropdown-item" href="{{route('magazine.create')}}">crea articolo</a></li>
                  <li><a class="dropdown-item" href="{{route('article.create')}}">crea annuncio</a></li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('form-logout').submit();">logout</a></li>
                  <form method="POST" action="{{route('logout')}}" id="form-logout">
                    @csrf
                  </form>
                </ul>
              </li>
              @endguest
            </ul>
          </div>
        </div>
    </nav>


    {{$slot}}


    <footer>
      <div class="container-fluid black-color mt-5 pt-3">
          <h2 class="text-center">Moto.Blog</h2>
          <div class="row">
              <div class="col-6 grey-color fw-lighter p-3">
                <p>Note Legali   Privacy</p>
                <p>2004-2021 Tutti i diritti sono riservati.</p>
               </div>
               <div class="col-6 grey-color text-center pt-4 ">
                  <i class="fab fa-twitter mx-3 icon-fouter"></i>
                  <i class="fab fa-facebook-square mx-3 icon-fouter"></i>
                  <i class="fab fa-google-plus-g mx-3 icon-fouter"></i>
                  <i class="fab fa-instagram mx-3 icon-fouter"></i>
              </div>
           </div>
      </div>
  
  </footer>
    
    
    
    
    <script src="{{asset('js/app.js')}}"></script>
</body>
</html>









