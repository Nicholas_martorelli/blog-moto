<x-layout>


    <header class="masthead pt-5 m-bot-100">
        <div class="container ">
          <div class="row  align-items-center">
            <div class="col-12 text-center">
              <h1 class="pt-5 fw-bold grey-color m-top-100">Il tuo Moto Blog preferito</h1>
              <p class="lead black-color">Il primo moto blog fatto da motociclisti per altri motociclisti!</p>
            </div>
          </div>
        </div>
    </header>

@if (session('message'))
	<div class="alert alert-success">
		{{session('message')}}
	</div>
@endif
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 text-center fw-bold black-color mb-5">
                <h2>Ecco cosa troverai nel nostro blog!</h2>
            </div>
            <div class="col-12 col-md-6 offset-md-3  card-home px-4 py-4">
              <h3 class="main-color">Articoli dettagliati sulle moto</h3>
              <p>Non sei molto esperto in moto? non c'è problema i nostri iscritti possono pubblicare articoli sui vari modelli di moto e dare le loro opinioni ed esperienze!</p>
              <a href="{{route('magazine.index')}}" class="btn button  ">vai agli articoli!</a>
           </div>
            <div class="col-12  col-md-6 offset-md-3 card-home px-4 py-4 my-5">
                <h3 class="main-color">Annunci di moto in vendita</h3>
                <p>Sei alla ricerca di un nuovo mezzo? qui troverai gli annunci dei nostri iscritti corri a dare un occhiata!</p>
                <a href="{{route('article.index')}}" class="btn button mt-5 ">vai agli annunci!</a>
            </div>
            <div class="container  m-top-100">
                <div class="card border-0 shadow my-5">
                  <div class="card-body p-5">
                    <h3 class="text-center fw-bold main-color mb-3">
                        Contattaci!
                    </h3>
                    <p class="text-center">Hai bisogno di aiuto?hai trovato qualcosa che non va? faccelo sapere!</p>
                    <div >
                        <form method="POST" action="{{route('submit')}}">
                            @csrf
                            <div class="mb-3">
                              <label for="exampleInputEmail1" class="form-label main-color">Nome Utente</label>
                              <input name="user"type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label main-color">Email</label>
                                <input name="email"type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                              </div>
                            <div class="mb-3">
                              <label for="exampleInputPassword1" class="form-label main-color">Descrizione</label>
                              <textarea name="message" class="form-control" id="exampleInputPassword1" cols="30" rows="7"></textarea>
                            </div>
                            <button type="submit" class="btn btn-success">Invia</button>
                        </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>























</x-layout>